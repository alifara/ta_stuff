// source: https://open.kattis.com/problems/narrowartgallery

#include <algorithm>
#include <array>
#include <iostream>

const int MAXN = 202;
const int MAXK = MAXN;

std::array<std::array<std::array<int, 2>, MAXK>, MAXN> dp;  // dp[MAXN][MAXK][2]
std::array<std::array<int, 2>, MAXN> room;                  // room[MAXN][2]
int N, K;

auto f(int n, int k, bool a)  // a -> left room or right room of i_th row
{
  if (k < 0)
    return std::numeric_limits<int>::min();
  if (n == 1 and k == 0)
    return room[n][a];
  if (n == 1)
    return std::numeric_limits<int>::min();

  int &u = dp[n][k][a];

  if (u != -1)
    return u;

  return u = std::max({
                 f(n - 1, k, a) + room[n - 1][!a],
                 f(n - 1, k - 1, a),
                 f(n - 1, k, !a) + room[n - 1][a],
             }) +
             room[n][a];
}

int main()
{
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);

  std::cin >> N >> K;
  N++;

  // room[0] = {0, 0}; // redundant
  for (int i = 1; i <= N; i++) {
    auto &u = room[i];
    std::cin >> u[0] >> u[1];
  }

  for (int i = 1; i <= N; i++)
    for (int j = 0; j <= K; j++)
      dp[i][j] = {-1, -1};

  std::cout << std::max(f(N, K, 0), f(N, K, 1)) << std::endl;
}
