# From: https://hexdocs.pm/elixir/1.14.3/GenServer.html

defmodule Counter do
  use GenServer

  @impl true
  def init(counter) do
    {:ok, counter}
  end

  @impl true
  def handle_call(:read, from, count) do
    # IO.puts("call(:read) from #{inspect(from)}")
    {:reply, count, count}
  end

  @impl true
  def handle_call(:inc, from, count) do
    # IO.puts("call(:inc)  from #{inspect(from)}")
    {:reply, count + 1, count + 1}
  end

  @impl true
  def handle_cast({:inc, i}, count) do
    {:noreply, count + i}
  end

  @impl true
  def handle_cast(:inc, count) do
    {:noreply, count + 1}
  end
end
