#!/usr/bin/env python
from typing import List, Tuple, Union

Skyline = List[Tuple[int, int]]
Buildings = List[Union[List[int], Tuple[int, int]]]


class Solution:
    def mergeSkyline(self, sk_left: Skyline, sk_right: Skyline) -> Skyline:
        sk = [(0, 0)]
        i = j = 0
        hi = hj = 0

        sk_left_len = len(sk_left)
        sk_right_len = len(sk_right)

        while (
            i < sk_left_len and j < sk_right_len
        ):  # continue until one skyline runs out
            if sk_left[i][0] < sk_right[j][0]:
                hi = sk_left[i][1]
                top = max(hi, hj)
                if sk[-1][1] != top:  # avoid duplicates
                    sk.append((sk_left[i][0], top))
                i += 1
            else:
                hj = sk_right[j][1]
                top = max(hi, hj)
                if sk[-1][1] != top:  # avoid duplicates
                    sk.append((sk_right[j][0], max(hi, hj)))
                j += 1

        # append the leftout skyline
        sk += sk_left[i:]
        sk += sk_right[j:]

        return sk

    def getSkyline(self, buildings: Buildings) -> Skyline:
        n = len(buildings)
        if n == 0:
            return []
        if n == 1:
            r = [(buildings[0][0], buildings[0][2]), (buildings[0][1], 0)]
            return r
        left = buildings[: n // 2]
        right = buildings[n // 2 :]
        return self.mergeSkyline(self.getSkyline(left), self.getSkyline(right))


def main():
    s = Solution()
    # buildings = [[2, 9, 10], [3, 7, 15], [5, 12, 12], [15, 20, 10], [19, 24, 8]]

    buildings_not = [
        [6, 0, 6, 8],
        [2, 0, 9, 5],
        [4, 0, 6, 7],
        [14, 0, 6, 6],
        [15, 0, 2, 9],
        [22, 0, 3, 6.5],
    ]
    buildings = list(map(lambda x: (x[0], x[0] + x[2], x[3]), buildings_not))
    sk = s.getSkyline(buildings)
    print(sk)

    S = 0
    for i in range(len(sk) - 1):
        S += abs((sk[i + 1][0] - sk[i][0]) * sk[i][1])
    print(S)


if __name__ == "__main__":
    main()
