#!/usr/bin/env python3
import networkx as nx
import numpy as np
from typing import Sequence, Dict


def _rotate_xyc(deg, xyc):
    def rotation_mat2D(deg):
        theta = np.radians(deg)
        c, s = np.cos(theta), np.sin(theta)
        return np.array(((c, -s), (s, c)))

    mat = rotation_mat2D(deg)
    for i, (x, y, c) in enumerate(xyc):
        x, y = np.array((x, y)) @ mat
        xyc[i] = (x, y, xyc[i][2])
    return xyc


def _make_tri(color, perturebed=False):
    y = 0.8 if perturebed else 0.5
    return [
        (1, 0, color),
        (2, y, color),  # the y should've been 0.5 but I perturbed it
        (2, -0.5, color),
    ]


G_3_tirs = [
    (0, 0, "coral"),
    *_rotate_xyc(90, _make_tri("aqua")),
    *_rotate_xyc(120 + 90, _make_tri("crimson")),
    *_rotate_xyc(90 - 120, _make_tri("lime")),
]

G_3_tirs_perturbed = [
    (0, 0, "coral"),
    *_rotate_xyc(90, _make_tri("aqua", perturebed=True)),
    *_rotate_xyc(120 + 90, _make_tri("crimson", perturebed=True)),
    *_rotate_xyc(90 - 120, _make_tri("lime", perturebed=True)),
]

G_simple = [
    (1, 0, "lime"),
    (0, 1, "lime"),
    (2, 1, "lime"),
    (2, 3, "crimson"),
    (0, 4, "crimson"),
    (1, 5, "crimson"),
    (2, 10, "crimson"),
    (2.5, 5, "aqua"),
]


def draw_graph(
    G,
    pos: Dict = None,
    round_edge=lambda x: round(x, 2),
    node_color=None,
    label: str = "weight",
    ax=None,
) -> None:
    if pos is None:
        pos = nx.circular_layout(G)  # pos = nx.nx_agraph.graphviz_layout(G)
    # pos = nx.spring_layout(G)  # pos = nx.nx_agraph.graphviz_layout(G)
    nx.draw_networkx(G, pos, node_color=node_color)
    if not label:
        return
    labels = nx.get_edge_attributes(G, label)
    for k in labels.keys():
        labels[k] = round_edge(labels[k])
    nx.draw_networkx_edge_labels(G, pos, edge_labels=labels, ax=ax)


def euclidian_dist(a: Sequence, b: Sequence) -> float:
    assert len(a) == len(b)
    return sum(map(lambda c: (c[0] - c[1]) ** 2, zip(a, b))) ** (1 / len(a))


def main():
    a = (1, 1)
    b = (0, 0)
    print(f"euclidian_dist({a}, {b}):\t{ euclidian_dist(a, b) }")


if __name__ == "__main__":
    main()
