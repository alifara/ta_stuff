## Dynamic Programming

* 1st problem is from [Partition problem geeksforgeeks](https://www.geeksforgeeks.org/partition-problem-dp-18/).

* 2nd and 4th problem are from [LightOJ 1232](http://www.lightoj.com/volume_showproblem.php?problem=1232) and [LightOJ 1360](http://lightoj.com/volume_showproblem.php?problem=1360).

  Testcases:

   * https://www.udebug.com/LOJ/1232
   * https://www.udebug.com/LOJ/1360
* 3rd problem is from [Sequence Alignment geeksforgeeks](https://www.geeksforgeeks.org/sequence-alignment-problem/).
* 5th problem is from [CPSC413 Quiz#7](http://pages.cpsc.ucalgary.ca/~eberly/Courses/CPSC413/1999/Tests/quiz_7.pdf).
