#+OPTIONS: toc:nil


* Problems
  :PROPERTIES:
  :UNNUMBERED: t
  :END:
- Find square {root, cube} using newton's method
- Fast exponentiation (\lg n)
- Freq. of each elements in a list
- Extract unique elements in a list
- Freq. and unique elems in a list
- Deep-reverse a list
- Deep-count the elems in a list
- Depth of a list
- Flatten a list
- Implement flat-map operation
- Is \chi a member of my list? :

  ='(1 2 3 (2 3))= ⊆ ='((1 2 3 (2 3)) 1 2 3 …)=
- Count parens:

  ='((()) ())= ≡ =2 + 1= \Rightarrow =3=
- Factorial (iterative {and, or, vs.} recursive)
- Merge operation and Merge sort
- Take the deravative of a polynominal:

  ='(2 8 2 0 1)= \Rightarrow ='(8 24 4 0)=

   =2x⁴ + 8x³ + 2x² + 0x + 1= \Rightarrow =8x³ + 24x²+ 4x  + 0=
- lin. alg. general pacakge:
  - multiply vector by numerial constant

  - vector dot vector:
   =[2 3 4]= × =[7 8 9]=
  - multiply vector by vector:
   =[2 3 4]= × =[7 8 9]=
  - multiply matrix by matrix
  - support primitives: 1 vector, eᵢ
- Binary search tree(not self-balancing)
- Print out Khayiam-Pascal triangle
  * =tri(3)=:

#+BEGIN_CENTER
       =1=

     =1 2 1=

    =1 3 3 1=
#+END_CENTER

- Split string using collection of chars(i.e. without regex):

  ~(clojure.string/split my-string #"[#,@,\,,:,x]")~
