# in IEx:

# give general info about type, most important of which is protocols it implements
i("asdf")

# kinda like docstring in Python
h("asdf")



# Ask the process to quit, gently
Process.exit(pid, :normal)

# Order the process to quit
Process.exit(pid, :kill)

# Self-explanatory
Process.info(pid)
