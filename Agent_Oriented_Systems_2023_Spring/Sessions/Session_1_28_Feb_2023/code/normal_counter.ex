defmodule NormalCounter do
  defstruct value: 0

  def start(initValue \\ 0) do
    %NormalCounter{value: initValue}
  end

  def inc(%NormalCounter{value: v}, i \\ 1) do
    new_value = v + i
    %NormalCounter{value: new_value}
  end

  def read(instance) do
    instance.value
  end
end

defmodule SimplerCounter do
  def start(initvalue \\ 0) do
    initvalue
  end

  def inc(v, i \\ 1) do
    v + i
  end

  def read(v) do
    v
  end
end
