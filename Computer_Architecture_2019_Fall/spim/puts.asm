.Ltext0:
.LFB3 = .
; main:
.LVL0 = .
.LBB2 = .
        blez    $4,.L8
        add $0,$0,$0
.LBE2 = .
        addiu  $sp,$sp,-32  #,,
        sd      $17,8($sp)   #,
        addiu   $17,$4,-1      # D.1734, argc,
        sll    $17,$17,32       # D.1737, D.1734,
        srl    $17,$17,32       # D.1737, D.1737,
        sd      $28,16($sp)  #,
        addiu  $17,$17,1    # D.1737, D.1737,
        lui     $28,%hi(%neg(%gp_rel(main)))       #,
        addu   $28,$28,$25    #,,
        sll    $17,$17,3        # D.1737, D.1737,
        sd      $16,0($sp)   #,
        sd      $31,24($sp)  #,
        addiu  $28,$28,%lo(%neg(%gp_rel(main)))     #,,
        move    $16,$5   # ivtmp.6, argv
        addu   $17,$5,$17     # D.1739, ivtmp.6, D.1737
.LVL1 = .
.LBB3 = .
        ld      $4,0($16)    #, MEM[base: _2, offset: 0B]
.L10:
        ld      $25,%call16(puts)($28)       # tmp247,,
1:      jalr        $25      # tmp247
        addiu  $16,$16,8    # ivtmp.6, ivtmp.6,

.LVL2 = .
        bnel    $16,$17,.L10     #, ivtmp.6, D.1739,
        ld      $4,0($16)    #, MEM[base: _2, offset: 0B]

.LBE3 = .
        ld      $31,24($sp)  #,
        ld      $28,16($sp)  #,
        ld      $17,8($sp)   #,
        ld      $16,0($sp)   #,
        move    $2,$0    #,
        j       $31    #
        addiu  $sp,$sp,32   #,,

.LVL3 = .
.L8:
        j       $31
        move    $2,$0    #,

.LFE3:
.Letext0:
.Ldebug_info0:
.Ldebug_abbrev0:
.Ldebug_loc0:
.LLST0:
.LLST1:
.LLST2:
.Ldebug_ranges0:
.Ldebug_line0:
.LASF20:
.LASF28:
.LASF23:
.LASF5:
.LASF7:
.LASF10:
.LASF33:
.LASF17:
.LASF12:
.LASF19:
.LASF24:
.LASF14:
.LASF51:
.LASF32:
.LASF6:
.LASF29:
.LASF45:
.LASF47:
.LASF44:
.LASF41:
.LASF1:
.LASF46:
.LASF4:
.LASF3:
.LASF42:
.LASF31:
.LASF16:
.LASF40:
.LASF13:
.LASF2:
.LASF11:
.LASF53:
.LASF43:
.LASF34:
.LASF35:
.LASF36:
.LASF37:
.LASF38:
.LASF50:
.LASF0:
.LASF18:
.LASF9:
.LASF8:
.LASF25:
.LASF22:
.LASF48:
.LASF27:
.LASF39:
.LASF15:
.LASF30:
.LASF21:
.LASF26:
.LASF49:
.LASF52:
