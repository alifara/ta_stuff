close all;
axis([0 25 0 10]);
rectangle('Position',[6 0 6 8],'FaceColor',[0.4940 0.1840 0.5560]);
rectangle('Position',[2 0 9 5],'FaceColor',[0.8500 0.3250 0.0980]);
rectangle('Position',[4 0 6 7],'FaceColor',[0.6350 0.0780 0.1840]);
rectangle('Position',[14 0 6 6],'FaceColor',[0.3010 0.7450 0.9330]);
rectangle('Position',[15 0 2 9],'FaceColor',[0.4660 0.6740 0.1880]);
rectangle('Position',[22 0 3 6.5],'FaceColor',[0.9290 0.6940 0.1250]);
figure 2;
axis([0 25 0 10]);
c = 'k';
rectangle('Position',[6 0 6 8],'FaceColor', c);
rectangle('Position',[2 0 9 5],'FaceColor', c);
rectangle('Position',[4 0 6 7],'FaceColor', c);
rectangle('Position',[14 0 6 6],'FaceColor', c);
rectangle('Position',[15 0 2 9],'FaceColor', c);
rectangle('Position',[22 0 3 6.5],'FaceColor', c);
figure 3;
axis([0 25 0 10]);
c = 'k';
rectangle('Position',[6 0 6 8],'FaceColor', c);
rectangle('Position',[2 0 9 5],'FaceColor', c);
rectangle('Position',[4 0 6 7],'FaceColor', c);
rectangle('Position',[14 0 6 6],'FaceColor', c);
rectangle('Position',[15 0 2 9],'FaceColor', c);
rectangle('Position',[22 0 3 6.5],'FaceColor', c);
pts = [[2 5]' [4 7]' [6 8]' [12 0]' [14 6]' [15 9]' [17 6]' [20 0]' [22 6.5]' [25 0]']
hold on;
for i = pts,
  x = i(1);
  y = i(2);
  plot(x, y, 'o-r');
endfor
