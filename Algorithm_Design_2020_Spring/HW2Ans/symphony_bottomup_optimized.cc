#include <bits/stdc++.h>

constexpr int MAXK = 100'010;
constexpr int MAXN = 110;

int N, K;
std::array<int, MAXK> dp;
std::array<int, MAXN> T;

static auto printDp() -> void
{
  std::cout << std::endl;
  for (int k = 0; k <= K; k++) {
    std::cout << dp[k] << ' ';
    // std::cout << std::endl;
  }
}

inline static auto modder(int a) -> int { return a % 100'000'007; }

static auto calc_comb(int length = K) -> int
{
  for (int i = 0; i < N; i++)
    for (int k = 1; k <= K; k++)
      if (T[i] <= k)
        dp[k] = modder(dp[k] + dp[k - T[i]]);

  return dp[length];
}

int main()
{
  int _, x = 0;

  dp[0] = 1;  // pre-condition, also resetDp() starts from index 1
  scanf("%d", &_);
  while (_--) {
    scanf("%d %d", &N, &K);
    for (int i = 0; i < N; i++)
      scanf("%d", &T.at(i));

    printf("Case %d: %d\n", ++x, calc_comb());
    // printDp();
    std::fill(std::begin(dp) + 1, std::begin(dp) + K + 1, 0);
  }
}
