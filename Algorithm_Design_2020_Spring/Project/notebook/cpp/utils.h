#pragma once

#include <iomanip>
#include <sstream>
#include <type_traits>

namespace daa {

  auto ss_round(const double val, const int precision) -> const double
  {
    std::stringstream tmp;
    tmp << std::setprecision(precision) << std::fixed << val;
    return stod(tmp.str());
  }

  auto ss_round(const float val, const int precision) -> const float
  {
    std::stringstream tmp;
    tmp << std::setprecision(precision) << std::fixed << val;
    return stod(tmp.str());
  }
}  // namespace daa
