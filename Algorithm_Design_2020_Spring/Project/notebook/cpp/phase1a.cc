#include <array>
#include <cassert>
#include <cmath>
#include <iostream>
#include <limits>
#include <utility>
#include <vector>

#include "cpptqdm/tqdm.h"
#include "utils.h"

using wtype = long long;

// // sanity stuff
// constexpr bool do_sanity = false;
// constexpr std::array<float, 4> ans{ 0.0f, 6538.0342f, 6475388.0f, 0.0f };
// constexpr int case_id = 2;
// constexpr float eps = 1e-6;

struct Edge {
  wtype w = std::numeric_limits<wtype>::max();
  int to  = -1;
};

inline wtype dist(const std::pair<wtype, wtype> u, const std::pair<wtype, wtype> v)
{
  wtype x, y;
  assert(!__builtin_sub_overflow(u.first, v.first, &x));
  assert(!__builtin_sub_overflow(u.second, v.second, &y));

  wtype ret;
  assert(!__builtin_mul_overflow(y, y, &y));
  assert(!__builtin_mul_overflow(x, x, &x));
  assert(!__builtin_add_overflow(x, y, &ret));
  if (ret < 0) {
    puts("AAAAAH");
    abort();
  }
  return ret;
}

float prim(const int n, const std::vector<std::pair<wtype, wtype>> &pos)
{
  std::vector<bool> selected(n, false);
  std::vector<Edge> min_e(n);
  double total_weight{};
  // tqdm bar;

  min_e[0].w = 0;

  for (int i = 0; i < n; i++) {
    int v = -1;

    for (int j = 0; j < n; ++j) {
      if (!selected[j] && (v == -1 || min_e[j].w < min_e[v].w))
        v = j;
    }

    if (min_e[v].w == std::numeric_limits<wtype>::max() || min_e[v].w < 0) {
      std::puts("No MST!");
      std::cout << min_e[v].w;
      exit(-1);
    }

    selected[v] = true;
    total_weight += std::sqrt(static_cast<double>(min_e[v].w)) / 1e4;

    for (int to = 0; to < n; to++) {
      wtype w;
      if ((w = dist(pos[v], pos[to])) < min_e[to].w)
        min_e[to] = {w, v};
    }

    // if (i % (1 << 8) == 0)
    //   bar.progress(i, n);

    // if (i % (1 << 11) == 0)
    //   printf("%f\n", total_weight);

    // if (do_sanity) {
    //   if (ans[case_id] - total_weight < -eps) { // sanity check for
    //   test-cases
    //     printf("sanity failed: %f\n", total_weight);
    //     exit(-2);
    //   }
    // }
  }
  // bar.finish();
  return total_weight;
}

int main()
{
  int n;

  std::scanf("%i", &n);

  std::vector<std::pair<wtype, wtype>> pos(n);
  for (auto &u : pos) {
    int _;
    float x, y;
    scanf("%f %f %i", &x, &y, &_);
    x *= 1e4;
    y *= 1e4;
    u = {static_cast<int>(x), static_cast<int>(y)};
  }

  float w = prim(n, pos);

  printf("%.2f", daa::ss_round(w, 2));
}
