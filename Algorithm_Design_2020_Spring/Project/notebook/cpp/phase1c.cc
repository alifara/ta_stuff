#include <array>
#include <cassert>
#include <cstdio>
#include <iostream>
#include <limits>
#include <map>
#include <numeric>
#include <utility>
#include <vector>

#include "cpptqdm/tqdm.h"
#include "utils.h"

// // sanity stuff
// constexpr bool do_sanity = false;
// constexpr std::array<float, 4> ans{ 0.0f, 6538.0342f, 6475388.0f, 0.0f };
// constexpr int case_id = 2;
// constexpr float eps = 1e-6;

// using wtype = long long;
using wtype = long double;

using Coord             = std::pair<wtype, wtype>;
using Component         = std::vector<Coord>;
using DisconnectedGraph = std::map<int, Component>;

constexpr auto max_comp_size = 10'000;
wtype adj_mat[max_comp_size][max_comp_size];

struct Edge {
  wtype w = std::numeric_limits<wtype>::max();
  int to  = -1;
};

inline wtype dist(const std::pair<wtype, wtype> u, const std::pair<wtype, wtype> v)
{
  // wtype x, y;
  // assert(!__builtin_sub_overflow(u.first, v.first, &x));
  // assert(!__builtin_sub_overflow(u.second, v.second, &y));

  // wtype ret;
  // assert(!__builtin_mul_overflow(y, y, &y));
  // assert(!__builtin_mul_overflow(x, x, &x));
  // assert(!__builtin_add_overflow(x, y, &ret));

  auto x   = u.first - v.first;
  auto y   = u.second - v.second;
  auto ret = x * x + y * y;

  if (ret < 0) {
    puts("AAAAAH");
    abort();
  }
  return ret;
}

// TODO: can be paralleized and vectorized.
auto floyd_warshall(const Component &comp) -> std::size_t
{
  auto n = comp.size();

  for (auto i = 0u; i < n; i++)
    for (auto j = i; j < n; j++)
      adj_mat[i][j] = std::sqrt(dist(comp[i], comp[j]));
  for (auto i = 0u; i < n; i++)        // Seperate trasnpose operations for
    for (auto j = 0u; j < i + 1; j++)  // better cache coherence which goes
      adj_mat[i][j] = adj_mat[j][i];   // right out of the window, here :(

  for (auto i = 0u; i < n; i++)  // Just to be sure
    adj_mat[i][i] = 0;

  /* Apparently, there is no need for Floyd-Warshall, but
   * without it, it finds different answers. */
  constexpr auto use_floyd_warshall_p = true;
  if (use_floyd_warshall_p) {
    // tqdm bar;
    for (auto k = 0u; k < n; ++k) {
      // bar.progress(k, n);
      for (auto i = 0u; i < n; ++i) {
        for (auto j = 0u; j < n; ++j) {
          wtype add;
          // assert(!__builtin_add_overflow(adj_mat[i][k], adj_mat[k][j],
          // &add));
          add           = adj_mat[i][k] + adj_mat[k][j];
          adj_mat[i][j] = std::min(adj_mat[i][j], add);
        }
      }
    }
    // bar.finish();
  }

  auto u    = -1;
  auto uval = std::numeric_limits<wtype>::max();
  for (auto i = 0u; i < n; i++) {
    auto maxidx = 0;
    auto maxval = std::numeric_limits<wtype>::min();
    for (auto j = 0u; j < n; j++)
      if (auto val = adj_mat[i][j]; val > maxval) {
        maxidx = i;
        maxval = val;
      }

    if (uval > maxval) {
      uval = maxval;
      u    = maxidx;
    }
  }

  assert(u != -1);
  return u;
}

int main()
{
  // if (argc < 2)
  // {ofile = std::std}
  int n;

  std::scanf("%i", &n);

  DisconnectedGraph comps;
  std::map<int, std::vector<int>> comp_idx_to_u;
  for (int i = 0; i < n; i++) {
    int state;
    wtype x, y;
    scanf("%Lf %Lf %i", &x, &y, &state);
    x *= 1e4;
    y *= 1e4;
    comps[state].emplace_back(x, y);
    comp_idx_to_u[state].emplace_back(i + 1);
    // u = {static_cast<int>(x), static_cast<int>(y)};
  }

  // tqdm bar;
  int i = 0, nbar = comps.size();
  // TODO: use std::map
  for (const auto &c : comps) {
    // bar.progress(i++, nbar);
    std::cout << c.first << ": " << comp_idx_to_u[c.first][floyd_warshall(c.second)] << '\n';
    std::cerr << "\r" << static_cast<float>(i++ * 100) / nbar;
  }
  // bar.finish();

  // for (auto u : comps[1])
  //   std::cout << u.first << ": " << u.second << ", ";
  // puts("");

  // float w = prim(comps);

  // printf("%.2f\n", daa::ss_round(w, 2));
}
