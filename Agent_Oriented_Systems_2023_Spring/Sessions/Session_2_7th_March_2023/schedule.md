# 2nd Session's Schedule

### REMINDER

Uncomment `IO.puts/1` statements

### Concurrency cont.
1. [GenServer](https://hexdocs.pm/elixir/1.14.3/GenServer.html)
   - callbacks -> [`@impl`](https://hexdocs.pm/elixir/1.14.3/Module.html#module-impl)
   - [`GenServer.start_link/2`](https://hexdocs.pm/elixir/1.14.3/GenServer.html#start_link/2) or [`GenServer.start/2`](https://hexdocs.pm/elixir/1.14.3/GenServer.html#start/2)
   - [`GenServer.call`](https://hexdocs.pm/elixir/1.14.3/GenServer.html#call/2)
   - [`GenServer.cast`](https://hexdocs.pm/elixir/1.14.3/GenServer.html#cast/2)

2. Multi-Node on the same machine

   `$ iex --sname '<<NODE_NAME>>'` OR
      
   `$ iex --sname '<<NODE_NAME@localhost>>'` OR
    
   `$ iex --cookie '<<SOME_SECRET>>' --sname ... etc`

   1. distributed mode
   2. `Node.start/1`
   3. `$ iex --sname 'something'`
      1. sname

         it's local
      2. cookie (optional)

3. Distributed mode

   from `tests.exs`
   
   - `Node.connect/1`
   - `Node.list/0`
   - `pid`
       - `Node.self/0`
       - The `pid` triplet
   - The `epmd` process

4. Local Process Registry

    ```elixir
    iex(1)> send self(), :hello
    :hello
    iex(2)> flush
    :hello
    :ok
    iex(3)> Process.register self, :me
    true
    iex(4)> send :me, :hello
    :hello
    iex(5)> flush
    :hello
    :ok
    OR
    iex(1)> {:ok, server} = GenServer.start(<<MODULE_NAME>>,
                                            <<INIT_VALUE>>,
                                            name: <<SOME_NAME>>)
                            ;; using builtin functionality
    ```

5. Global Process Registry (Erlang's)
    - on server

    ```elixir
    iex(server@FQDN)n-1> Node.self()
    server@FQDN
    iex(server@FQDN)n> :global.register_name(<<SOME_NAME>>, 
                                             <<WHATEVER (maybe SERVER_PID?)>>) 
                                             ;; manual
    OR
    iex(server@FQDN)n> {:ok, server} = GenServer.start(<<MODULE_NAME>>,
                                                       <<INIT_VALUE>>,
                                                       name: {:global, <<SOME_NAME>>})
                                       ;; using builtin functionality
    ```

    - on client

    ```elixir
    iex(client@FQDN)n-1> Node.connect(<<some_other_node_on_network@FQDN>>)
    true
    iex(client@FQDN)n> s = :global.whereis_name(<<SOME_NAME>>)
    <<AN INSTANCE OF A REMOTE PID OBJECT>>
    iex(client@FQDN)n+1> GenServer.call(s, {:lookup, :somekey})
    nil
    iex(client@FQDN)n+2> GenServer.cast(s, {:insert, :somekey, :somevalue})
    :ok
    iex(client@FQDN)n+3> GenServer.call(s, {:lookup, :somekey})
    :somevalue
    ```
 
6. Distributed mesh on multiple machines

    ```bash
    $ iex --name 'faraz@172.22.196.227' \
          --cookie 'secret_lol' \
          --erl '-kernel inet_dist_listen_min 60000' \
          --erl '-kernel inet_dist_listen_max 61000'
    ```
    ```bash
    $ ss -tlpn # list (t)CP ports in (l)ISTEN state with (p)rocess name and port (n)umbers
    ```

   - Setting up

      1. get ip `$ ip -br a`

      2. firewall

         - defaults: **deny (incoming), allow (outgoing), disabled (routed)**

         - allow others to connect to BEAM-VM

         <!-- hash symbol as shell prompt is rendered as line comment on forgejo :((((( -->
            ```bash
            $ sudo ufw allow proto tcp from <<SUBNET (IN CIDR COMPACT NOTATION)>> \
                       to any port <<PORT_START>>:<<PORT_END>> comment 'beam.smp'
            ```

         - allow others to connect to 

            ```bash
            $ sudo ufw allow proto tcp from <<SUBNET (IN CIDR COMPACT NOTATION)>> \
                       to any port 4369 comment 'epmd'
            ```

      3. `--name`
      4. `inet_dist_listen_min` and `inet_dist_listen_max`

   - Example
      - On Server

         ```elixir
         iex(server@FQDN)n> Node.self()
         server@FQDN
         iex(server@FQDN)n> c "stuff_dictionary.ex"
         [StuffDictionary]
         iex(server@FQDN)n+1> {:ok, server} = GenServer.start(StuffDictionary, 
                                                              %{}, 
                                                              name: {:global, :sdict})
         ```

      - On client

         ```elixir
         iex(client@FQDN)n> Node.connect(:"server@FQDN") ; or some other node
         true
         iex(client@FQDN)n> s = :global.whereis_name(:sdict)
         <<AN INSTANCE OF A REMOTE PID OBJECT>>
         iex(client@FQDN)n+1> GenServer.call(s, {:lookup, :somekey})
         nil
         iex(client@FQDN)n+2> GenServer.cast(s, {:insert, :somekey, :somevalue})
         :ok
         iex(client@FQDN)n+3> GenServer.call(s, {:lookup, :somekey})
         :somevalue
         ```

7. `$ pkill epmd` and close opened ports on firewall
