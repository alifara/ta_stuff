Code.require_file("normal_counter.ex")
ExUnit.start()

defmodule CounterTest do
  use ExUnit.Case, async: true

  def test_counter(counter) do
    v =
      counter.start(8)
      |> counter.inc()
      |> counter.inc(-8)
      |> counter.inc(0)
      |> counter.inc(8)
      |> counter.read()

    v == 9
  end

  test "NormalCounter" do
    assert true == test_counter(NormalCounter)
  end

  test "SimplerCounter" do
    assert true == test_counter(SimplerCounter)
  end
end
