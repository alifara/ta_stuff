You are given $n$ nodes, $N_0, ..., N_{n-1}$.

The first node $N_0$ is a master node, and the rest are compute-slave nodes.

You are tasked with creating a distributed-system that enables processes outside this system, foreign processes, to submit compute-tasks to $P_{rendezvous}$, and request the results back.

The process $P_{rendezvous}$ resides on $N_0$, and is responsible for distributing compute-tasks among compute nodes.

A compute-task is made of a single operation and some data. An operation is either a [`sort`](https://hexdocs.pm/elixir/1.14.5/Enum.html#sort/1), a [`gcd`](https://en.wikipedia.org/wiki/Greatest_common_divisor), a [`regex_replace`](https://hexdocs.pm/elixir/1.14.5/Regex.html#replace/4), or a `create_compute_process_on_compute_node()`.

$P_{rendezvous}$ also keeps track of compute-processes, which are created on compute nodes at the request of foreign processes, via `create_compute_process_on_compute_node()`.

When a foreign process issues a `create_compute_process_on_compute_node()`, $P_{rendezvous}$ will create a compute-process on the compute node with the least number of compute-processes. If there are multiple such nodes, pick any.

Note that foreign processes cannot directly communicate with any process on any compute node.
They submit their tasks to $P_{rendezvous}$ only.

---

Here's an example piece of Elixir code that a foreign process may run, line by line:

```elixir
n0 = YourSystem.get_master_pid()
{:failure, cause} = YourSystem.sort(n0, [3, 2 ,1]) # no compute-process available yet
{:success} = YourSystem.create_compute_process_on_compute_node(n0)
{:success, [1, 2, 3]} = YourSystem.sort(n0, [3, 2 ,1])
{:success, 24} = YourSystem.gcd(n0, 72, 96)
{:success, "adc"} = YourSystem.regex_replace(n0, ~r/b/, "abc", "d")
```

---

Here's a diagram demonstrating how different components of this system may interact with each other:

```mermaid
flowchart LR
  subgraph Compute-Module
    direction LR
    subgraph Master-Node-N0
        direction TB
        P_rendezvous
    end
    subgraph Compute-Node-N1
        direction TB
        P0-N1
        P1-N1
    end
    subgraph Compute-Node-N2
        direction TB
        P0-N2
    end
    subgraph Compute-Node-N3
        direction TB
        P0-N3
        P1-N3
        P2-N3
    end
  end
  subgraph Outside-World
      direction LR
      Some-Foreign-Process
  end
  P_rendezvous <--> P0-N1
  P_rendezvous <--> P1-N1
  P_rendezvous <--> P0-N2
  P_rendezvous <--> P0-N3
  P_rendezvous <--> P1-N3
  P_rendezvous <--> P2-N3
  Some-Foreign-Process <-- Compute-Task --> P_rendezvous
```
