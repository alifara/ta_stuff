defmodule  StuffDictionary do
  use GenServer

  @impl true
  def init(init_state) do
    {:ok, init_state}
  end

  @impl true
  def handle_call({:lookup, key}, from, state) do
	# IO.puts("keylookup #{inspect key} from #{inspect(from)}")
    {:reply, state[key], state}
  end

  @impl true
  def handle_cast({:insert, key, value}, state) do
    new_state = Map.put(state, key, value)
    {:noreply, new_state}
  end
end
