#!/usr/bin/env python
from typing import Sequence, Tuple


class Solution:
    def merge(self, left: Sequence, right: Sequence) -> Tuple[int, Sequence]:
        i = j = 0
        nl = len(left)
        nr = len(right)
        cnt = 0
        res = []

        while i < nl and j < nr:
            if left[i] <= right[j]:
                res.append(left[i])
                i += 1
            else:
                # increment by the amount of left's elems in res
                cnt += nl - i
                res.append(right[j])
                j += 1

        # copy remaining elems
        res += left[i:]
        res += right[j:]

        return cnt, res

    def getInversionCount(self, seq: Sequence) -> Tuple[int, Sequence]:
        n = len(seq)
        if n <= 1:
            return 0, seq

        left = seq[: n // 2]
        right = seq[n // 2 :]

        cnt_l, sorted_l = self.getInversionCount(left)
        cnt_r, sorted_r = self.getInversionCount(right)
        cnt_t, sorted_t = self.merge(sorted_l, sorted_r)

        return (cnt_l + cnt_r + cnt_t), sorted_t


def main():
    s = Solution()
    v = [1, 3, 2, 3, 1]
    print(s.getInversionCount(v)[0])


if __name__ == "__main__":
    main()
