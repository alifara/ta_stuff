## Divide and Conquer

* 1st and 7th problem are from [Stanford's 2nd Homework](http://www-leland.stanford.edu/class/cs161/Homework/Homework2/).
* 2nd problem is from [Introduction-to-Algorithms-A-Creative-Approach, by Udi Manber](https://www.worldcat.org/title/introduction-to-algorithms-a-creative-approach/oclc/294948010).
* 3rd problem is from [leetcode.com's divide-and-conquer problems](https://leetcode.com/tag/divide-and-conquer/).
* 4th, 5th, and 6th problems are from Jon Kleinberg and Éva Tardos's book, [Algorithm Design](https://www.worldcat.org/title/algorithm-design/oclc/874808893).

