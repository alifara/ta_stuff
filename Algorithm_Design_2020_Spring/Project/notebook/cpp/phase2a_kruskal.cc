#include <array>
#include <cassert>
#include <cmath>
#include <iostream>
#include <limits>
#include <map>
#include <numeric>
#include <utility>
#include <vector>

#include "cpptqdm/tqdm.h"
#include "utils.h"

// // sanity stuff
// constexpr bool do_sanity = false;
// constexpr std::array<float, 4> ans{ 0.0f, 6538.0342f, 6475388.0f, 0.0f };
// constexpr int case_id = 2;
// constexpr float eps = 1e-6;

using wtype = int64_t;     // edge weight's type
using utype = int32_t;     // node index type, should be of integer family
                           // c++20 concepts pls
using ctype    = int32_t;  // componet(state)'s type
using costtype = double;   // mcst's cost type

using Coord             = std::pair<wtype, wtype>;
using Component         = std::vector<Coord>;
using DisconnectedGraph = std::map<int, Component>;

struct Edge {
  wtype w = std::numeric_limits<wtype>::max();
  int to  = -1;
};

using KruskalEdge = std::pair<wtype, std::pair<utype, utype>>;

inline wtype dist(const std::pair<wtype, wtype> u, const std::pair<wtype, wtype> v)
{
  wtype x, y;
  assert(!__builtin_sub_overflow(u.first, v.first, &x));
  assert(!__builtin_sub_overflow(u.second, v.second, &y));

  wtype ret;
  assert(!__builtin_mul_overflow(y, y, &y));
  assert(!__builtin_mul_overflow(x, x, &x));
  assert(!__builtin_add_overflow(x, y, &ret));
  if (ret < 0) {
    puts("AAAAAH, THIS SHOULD NOT HAVE HAPPEND");
    abort();
  }
  return ret;
}

auto ds_find(std::vector<utype> &par, utype u) -> utype
{
  if (par[u] == u)
    return u;
  return par[u] = ds_find(par, par[u]);
}

auto ds_union(std::vector<utype> &par, std::vector<utype> &rank, utype u, utype v) -> void
{
  utype uu = ds_find(par, u);
  utype vv = ds_find(par, v);
  if (rank[uu] == rank[vv]) {
    ++rank[uu];
    par[uu] = vv;
  } else if (rank[uu] < rank[vv])
    par[vv] = uu;
  else  // same height, select one at random
    par[uu] = vv;
}

auto primTotal(const std::vector<Coord> &vertices, std::map<ctype, std::vector<utype>> &comp_to_u,
               const std::vector<ctype> &u_to_comp) -> double
{
  // why can't I mark an std::map as const and be able to subscript it?
  auto n = vertices.size();
  std::vector<bool> selected(n, false);
  std::vector<Edge> min_e(n);
  double total_weight{};
  // tqdm bar;
  /////

  std::vector<utype> ds_rank(n, 0);
  std::vector<utype> ds_partition(n);
  std::generate(std::begin(ds_partition), std::end(ds_partition), [n = 0]() mutable { return n++; });

  std::vector<KruskalEdge> edges;
  auto comp_size = 100;
  for (auto i = 0; i < n / comp_size; i++)
    for (auto j = i + 1; j < n / comp_size; j++) {
      for (auto u : comp_to_u[i + 1])
        for (auto v : comp_to_u[j + 1]) {
          KruskalEdge e = {dist(vertices[u], vertices[v]), {u, v}};
          edges.push_back(e);
        }
    }
  std::cout << "Edges done\n";

  std::sort(std::begin(edges), std::end(edges));
  std::cout << "Sort done\n";

  auto unin = [&](utype u, utype v) { ds_union(ds_partition, ds_rank, u, v); };
  auto find = [&](utype u) { return ds_find(ds_partition, u); };

  for (auto i = 0; i < n / comp_size; i++) {
    auto v = comp_to_u[i + 1].at(0);
    for (auto u : comp_to_u[i + 1])
      if (find(u) != find(v))
        unin(u, v);
  }
  std::cout << comp_to_u

      auto mcstSize = 0u;
  double mcstCost   = 0.0;
  for (auto i = 0u; i < edges.size(); i++) {
    auto [w, uv] = edges[i];
    auto [u, v]  = uv;
    if (find(u) != find(v)) {
      unin(u, v);
      mcstCost += std::sqrt(static_cast<double>(w)) / 1e4;
      mcstSize++;
    }
  }
  return mcstCost;
  // ///////////
  // // Set the first component(component 1) to all zeros
  // for (const auto u : comp_to_u[1])
  //   min_e[u].w = 0;
  // // min_e[0].w = 0;

  // for (auto i = 0u; i < n; i++) {
  //   int v = -1;

  //   for (auto j = 0u; j < n; ++j) {
  //     if (!selected[j] && (v == -1 || min_e[j].w < min_e[v].w))
  //       v = j;
  //   }

  //   if (v == -1) {
  //     // bar.finish();
  //     return total_weight;
  //   }

  //   if (min_e[v].w == std::numeric_limits<wtype>::max() || min_e[v].w < 0) {
  //     std::puts("No MST!");
  //     std::cout << min_e[v].w;
  //     exit(-1);
  //   }

  //   // mark whole component as seen.
  //   for (const auto u : comp_to_u[u_to_comp[v]])
  //     selected[u] = true;
  //   // selected[v] = true;
  //   total_weight += std::sqrt(static_cast<double>(min_e[v].w)) / 1e4;

  //   for (auto to = 0u; to < n; to++) {
  //     wtype w;
  //     // std::cout << v << ", " << to << std::endl;
  //     if ((w = dist(vertices[v], vertices[to])) < min_e[to].w)
  //       min_e[to] = {w, v};
  //   }

  //   // if (i % (1 << 8) == 0)
  //   //// bar.progress(i, n);
  //   // if (i % (1 << 11) == 0)
  //   // printf("%f\n", total_weight);

  //   // if (do_sanity) {
  //   //   if (ans[case_id] - total_weight < -eps) { // sanity check for
  //   //   test-cases
  //   //     printf("sanity failed: %f\n", total_weight);
  //   //     exit(-2);
  //   //   }
  //   // }
  // }
  // // bar.finish();
  // return total_weight;
}

auto kruskalComponent(const std::vector<Coord> &vertices) -> double
{
  auto n = vertices.size();
  auto m = n * (n - 1) / 2;

  std::vector<KruskalEdge> edges(m);

  std::vector<utype> ds_rank(n, 0);
  std::vector<utype> ds_partition(n);
  std::generate(std::begin(ds_partition), std::end(ds_partition), [n = 0]() mutable { return n++; });

  {  // create edges for the component
    auto k = 0u;

    for (auto i = 0u; i < n; i++) {
      for (auto j = i + 1u; j < n; j++) {
        edges[k++] = {dist(vertices[i], vertices[j]), {i, j}};
      }
    }
    assert(k == m);
  }

  std::sort(std::begin(edges), std::end(edges));

  auto unin = [&](utype u, utype v) { ds_union(ds_partition, ds_rank, u, v); };
  auto find = [&](utype u) { return ds_find(ds_partition, u); };

  auto mcstSize   = 0u;
  double mcstCost = 0.0;
  for (auto i = 0u; i < m && mcstSize < n; i++) {
    auto [w, uv] = edges[i];
    auto [u, v]  = uv;
    if (find(u) != find(v)) {
      unin(u, v);
      mcstCost += std::sqrt(static_cast<double>(w)) / 1e4;
      mcstSize++;
    }
  }

  return mcstCost;
}

int main()
{
  int n;

  std::scanf("%i", &n);

  DisconnectedGraph comp_to_xy;
  std::map<ctype, std::vector<utype>> comp_to_u;
  std::vector<ctype> u_to_comp(n);
  std::vector<Coord> vertices(n);
  for (int i = 0; i < n; i++) {
    int state;
    float x, y;
    scanf("%f %f %i", &x, &y, &state);
    x *= 1e4;
    y *= 1e4;
    vertices[i] = {static_cast<int>(x), static_cast<int>(y)};
    comp_to_xy[state].emplace_back(x, y);
    comp_to_u[state].emplace_back(i);
    u_to_comp[i] = state;
  }

  // for (int i = 0; i < vertices.size(); i++)
  //   std::cout << vertices[i].first << ", " << vertices[i].second << ", "
  //             << u_to_comp[i] << '\n';

  auto connectCompCost = primTotal(vertices, comp_to_u, u_to_comp);
  // std::cout << "Total MCST:\n" << connectCompCost << '\n';

  // std::cout << "Component MCST:\n";
  tqdm barComp;
  // TODO: use std::map
  std::map<ctype, double> comp_to_cost_mcst;
  for (  // int i = 0, nbar = comp_to_xy.size();
      const auto &c : comp_to_xy) {
    // barComp.progress(i++, nbar);
    // std::cout <<
    (comp_to_cost_mcst[c.first] = kruskalComponent(c.second))
        // << '\n'
        ;
    // std::cerr << "\r" << static_cast<float>(i++ * 100) / nbar;
  }
  // barComp.finish();

  double totalRoadConstructionCost = connectCompCost;
  for (auto cost : comp_to_cost_mcst)
    totalRoadConstructionCost += cost.second;

  printf("%.2f", daa::ss_round(static_cast<float>(totalRoadConstructionCost), 2));
}
