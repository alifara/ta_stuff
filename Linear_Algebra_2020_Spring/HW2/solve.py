#!/usr/bin/env python
# import numpy as np
# from random import uniform, shuffle
# import sys


def find_loop_len(i, p):
    idxs = [i]
    nxt = p[i]
    while nxt != i:
        idxs.append(nxt)
        nxt = p[nxt]
    return idxs


def createEvenLoop(A, l1, l2):
    assert len(l1) == len(l2)
    assert len(l1) % 2 == 0

    loop = [None] * (2 * len(l1))
    loop[::2] = l1
    loop[1::2] = l2
    for i in range(len(loop) - 1):
        A[loop[i]] = loop[i + 1]
    A[loop[-1]] = loop[0]


def createOddLoop(A, loop):
    assert len(loop) % 2 == 1

    n = len(loop)
    if n == 1:
        A[loop[0]] = loop[0]
        return

    old = 0
    next = 2
    while next != 0:
        A[loop[old]] = loop[next]
        old = next
        next = (next + 2) % n
    A[loop[-2]] = loop[0]


def perm_sqrt(p):
    n = len(p)
    loops = [[] for i in range(n + 1)]
    seen_idx = set()

    for i in range(n):
        if i in seen_idx:
            continue
        idxs = find_loop_len(i, p)
        loops[len(idxs)] += [idxs]
        for i in idxs:
            seen_idx.add(i)

    # check if square-root exists
    for i in range(1, n + 1):
        if i % 2 == 0:
            if len(loops[i]) % 2 != 0:
                return []

    A = [-1 for i in range(n)]
    for i in range(1, n + 1):
        if i % 2 == 0:
            for j in range(0, len(loops[i]), 2):
                createEvenLoop(A, loops[i][j], loops[i][j + 1])
        else:
            for j in range(len(loops[i])):
                createOddLoop(A, loops[i][j])
    return A


def main():
    input()  # Who needs N any way when you got python.
    x = list(map(int, input().split()))
    A = perm_sqrt(x)
    if A:
        print(*A)
    else:
        print("Impossible")
    # print()  # for pedantic output
    return

    # def is_permuation_matrix(x):
    #     x = np.asanyarray(x)
    #     return (x.ndim == 2 and x.shape[0] == x.shape[1] and
    #             (x.sum(axis=0) == 1).all() and
    #             (x.sum(axis=1) == 1).all() and
    #             ((x == 1) | (x == 0)).all())

    # p_root = np.zeros((n,n), dtype=np.int)
    # for i in range(len(A)):
    #     p_root[i][A[i]] = 1
    # # print(p_root)
    # p = np.matmul(p_root, p_root)
    # if is_permuation_matrix(p_root) and is_permuation_matrix(p):
    #     print(*A)
    #     print()
    # else:
    #     print("Wrong", file=sys.stderr)
    #     exit(-1)


if __name__ == "__main__":
    t = int(input())
    for t in range(t):
        main()
