# From: https://hexdocs.pm/elixir/1.14.3/Agent.html

defmodule CounterAgent do
  use Agent

  def start_link(intial_value \\ 0) do
    Agent.start_link(fn -> intial_value end, name: __MODULE__)
  end

  def inc(i \\ 1) do
    # Agent.update(__MODULE__, fn a -> a + i end)
    Agent.update(__MODULE__, &(&1 + i))
  end

  def read do
    # Agent.get(__MODULE__, fn a -> a end)
    Agent.get(__MODULE__, & &1)
  end
end
