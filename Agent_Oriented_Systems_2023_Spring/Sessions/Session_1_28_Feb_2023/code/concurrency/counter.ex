defmodule Counter do
  def start(initValue \\ 0) do
    spawn(fn -> loop(initValue) end)
  end

  defp loop(state) do
    new_state =
      receive do
        {:inc, a} ->
          state + a

        {:read, caller} ->
          send(caller, {:value, state})
          state

        # ignore other msgs, frees up process's msg queue
        _ ->
          state
      end

    loop(new_state)
  end

  def inc(server, i \\ 1) do
    send(server, {:inc, i})
  end

  def read(server) do
    send(server, {:read, self()})

    receive do
      # or {:ok, v}
      {:value, v} -> v
      _ -> nil
    after
      5000 -> {:error, :timeout}
    end
  end

  def read(server, receiver) do
    send(server, {:read, receiver})
  end
end

# FIXME:
# --1. in Counter.read/1 -> what if multiple :atoms?--
# --2. Do I really have do a receive in read/0?--
# --3. Aren't read/0 and read/1 the same?--
# 4. inc/2 shouldn't return send's msg, thus leaking internal implementation details
# 5. Counter can be implemented a lot easier and clearer using Agent.
