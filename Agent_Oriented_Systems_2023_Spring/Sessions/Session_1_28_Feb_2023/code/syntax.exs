#!/usr/bin/env elixir

defmodule MyModule do
  def normal_func(a, b) do
    a + b
  end

  # nothing special
  def inline_func(a, b), do: a + b
end

IO.puts(MyModule.normal_func(1, 2))
IO.puts(MyModule.inline_func(1, 2))

defmodule SomeFunctionality do
  defp func_impl(a, b, c) do
    a * b + c
  end

  # default args
  def func_interface(a, b, c \\ 0) do
    func_impl(a, b, c)
  end
end

# # ** (UndefinedFunctionError) function SomeFunctionality.func_impl/3 is undefined or private
# IO.puts(SomeFunctionality.func_impl(2, 3, 5))

IO.puts(SomeFunctionality.func_interface(2, 3, 5))

f = fn -> "hello" end
g = fn name -> "hello " <> name end

# notice the dot
IO.puts(f.())
# what happend to the parens?
IO.puts(g.("ali"))
# what?! # sidenote: I love this operator!
g.("pipe operator") |> IO.puts()
