#!/usr/bin/env elixir

defmodule MultipleFunctions do
  def f(a) do
    IO.puts("called f(a)")
    a
  end

  def f(a, b) do
    IO.puts("called f(a, b)")
    r = [a, b + a]
    # a list
    IO.inspect(r)
    r
  end

  def f(a, b, c) do
    IO.puts("called f(a, b, c)")
    IO.inspect([a, b, c])
  end
end

# How does this make any sense? (hint: pattern matching baby!)
1 = MultipleFunctions.f(1)

IO.puts(MultipleFunctions.f(1, 2))
IO.puts(MultipleFunctions.f(1, 2, 3))

IO.puts("#####################################################################")

x = 1
1 = x

{a, b, c} = {:hello, "world", 42}

# another way of writing lambdas
Enum.map([a, b, c], &IO.puts(&1))
# same as  Enum.map([a, b, c], fn x -> IO.puts(x) end)

defmodule MultipleFunctionsPatternMatching do
  def f({:add, a, b}) do
    a + b
  end

  def f({:sub, a, b}), do: f({:add, a, -b})
  def f({:mul, a, b}), do: a * b
  def f({:div, a, b}), do: div(a, b)

  def run_example_on(a \\ 3, b \\ 5) do
    IO.puts("BEGIN run_example_on")
    Enum.map([:add, :sub, :mul, :div], fn op ->
      res = MultipleFunctionsPatternMatching.f({op, a, b})
      IO.puts("\tf({:#{op}, #{a}, #{b}) -> #{res}")
    end)
    IO.puts("END   run_example_on")
    true # everything was ok
  end
end

true = MultipleFunctionsPatternMatching.run_example_on()
