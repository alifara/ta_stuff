#include <bits/stdc++.h>

constexpr int MAXM = 200'000;

std::vector<int> start[MAXM];
std::vector<int> cost[MAXM];

std::array<int, MAXM> dp;
int max_idx;

static auto advertise(int idx = 0) -> int
{
  if (idx >= max_idx)  // no ads beyond this point
    return 0;

  if (dp[idx] != -1)
    return dp[idx];

  int with = 0, without = 0;

  for (size_t i = 0; i < start[idx].size(); i++) {
    with = std::max(with, cost[idx][i] + advertise(idx + start[idx][i]));
  }

  without = advertise(idx + 1);

  {  // cleanup for the next testcase
    start[idx].clear();
    cost[idx].clear();
  }

  return dp[idx] = std::max(with, without);
}

int main()
{
  int t = 0, _;
  scanf("%d", &_);

  while (_--) {
    int n;
    scanf("%d", &n);

    max_idx = 0;
    for (int i = 0; i < n; i++) {
      int a, b, c;
      scanf("%d %d %d", &a, &b, &c);
      start[a].push_back(b);
      cost[a].push_back(c);
      max_idx = std::max(max_idx, a + b - 1);
    }

    std::fill(std::begin(dp), std::end(dp), -1);

    printf("Case %d: %d\n", ++t, advertise());
  }
}
