Code.require_file("counter.ex")
Code.require_file("counter_with_agent.ex")

ExUnit.start()

defmodule CounterTest do
  use ExUnit.Case, async: true

  test "Start server" do
    _server = Counter.start()
  end

  test "inc + read" do
    server = Counter.start()
    Counter.inc(server)
    assert 1 == Counter.read(server)
  end

  test "start/1 + inc/2 + read/2" do
    a = 3
    b = 4

    server = Counter.start(a)
    Counter.inc(server, b)
    Counter.read(server, self())

    result =
      receive do
        {:value, v} -> v
      after
        500 -> {:error, :timeout}
      end

    assert a + b == result
  end

  test "test if it's resilient to shitty msgs" do
    a = 300
    b = 4

    server = Counter.start(a)
    Counter.inc(server, b)

    send(server, :bullshit)

    assert a + b == Counter.read(server)

    Counter.inc(server, -b)

    assert a == Counter.read(server)
  end
end

defmodule CounterAgentTest do
  use ExUnit.Case, async: true

  test "Start server" do
    {state, _pid} = CounterAgent.start_link()
    assert state == :ok
  end

  test "read" do
    expected = 20
    {:ok, _pid} = CounterAgent.start_link(expected)
    assert expected == CounterAgent.read()
  end

  test "start/1 + inc/2 + read/2" do
    initialValue = 20
    expected = 30

    {:ok, _pid} = CounterAgent.start_link(initialValue)
    assert :ok == CounterAgent.inc()
    assert :ok == CounterAgent.inc(9)

    assert expected == CounterAgent.read()
  end
end
