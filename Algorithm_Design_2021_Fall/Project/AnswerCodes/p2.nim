import strscans
import deques, heapqueue
import arraymancer

let
  MAXN = 1000
  MAXM = 1000

var
  grid = newTensor[int]([MAXN, MAXM])
  seen = newTensor[bool]([MAXN, MAXM])


func bfs(n, m: int, grid: var Tensor[int], queue: var Deque[(int, int)]): int =
  var time = 1
  var ended: bool
  while queue.len != 0:
    let (i, j) = queue.popFirst
    if i == j and j == -1:
      if ended:
        break
      else:
        inc time
        queue.addLast (-1, -1)
        ended = true
        continue
    for di in -1..1:
      for dj in -1..1:
        if not (i == j and j == 0):
          let
            ii = i + di
            jj = j + dj
          if 0 <= ii and 0 <= jj and i < n and j < m and grid[ii, jj] == -1:
            grid[ii, jj] = time
            ended = false
            queue.addLast (ii, jj)
  return time - 1

func dijkstra(n, m, k: int, src, trgt: (int, int),
              grid: var Tensor[int],
              seen: var Tensor[bool]): int =
  result = -1
  var hq = toHeapQueue [(0, src[0], src[1])]

  while hq.len != 0:
    let (time, x, y) = hq.pop
    if seen[x, y]:
      continue
    seen[x, y] = true

    if x == trgt[0] and y == trgt[1]:
      result = time
      break

    for (dx, dy) in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
      let
        xx = x + dx
        yy = y + dy
      if 0 <= xx and 0 <= yy and x < n and y < m and not(seen[xx, yy]) and
          time < grid[xx, yy] * k - 1:
        hq.push (time + 1, xx, yy)

proc f(n, m, k: int) =
  var k = k
  var queue: Deque[(int, int)]
  var src, trgt: (int, int)

  block getInput:
    for i in 0..<n:
      let line = stdin.readLine
      for j in 0..<m:
        seen[i, j] = false
        case line[j]:
          of 'f':
            grid[i, j] = 0
            queue.addLast (i, j)
          of 's':
            grid[i, j] = -1
            src = (i, j)
          of 't':
            grid[i, j] = -1
            trgt = (i, j)
          else:
            grid[i, j] = -1

  if queue.len == 0: #  no fire
    k = - (n + m) #  make the fire so slow that it would only advance after a long time
  else:
    queue.addLast (-1, -1)
    discard bfs(n, m, grid, queue)
  let d = dijkstra(n, m, k, src, trgt, grid, seen)
  echo if d == -1: "Impossible" else: $d

when isMainModule:
  var m, n, k: int
  while scanf(stdin.readLine, "$i $i $i", n, m, k) and m != 0 and n != 0 and k != 0:
    f(n, m, k)
