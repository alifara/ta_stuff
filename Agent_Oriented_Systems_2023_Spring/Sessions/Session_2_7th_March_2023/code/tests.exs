#!/usr/bin/env elixir

Code.require_file("counter_with_genserver.ex")
Code.require_file("stuff_dictionary.ex")
ExUnit.start()

defmodule CounterTest do
  use ExUnit.Case, async: true

  def popcount(n), do: for(<<bit::1 <- :binary.encode_unsigned(n)>>, do: bit) |> Enum.sum()

  test "Start server" do
    {:ok, server} = GenServer.start(Counter, 0)
    assert true == Process.alive?(server)
  end

  test "Read count" do
    {:ok, server} = GenServer.start(Counter, 8)
    assert 8 == GenServer.call(server, :read)
  end

  test "Increment counter with cast" do
    {:ok, server} = GenServer.start(Counter, 0)
    :ok = GenServer.cast(server, {:inc, 3})
    :ok = GenServer.cast(server, :inc)
    assert 4 == GenServer.call(server, :read)
  end

  test "Increment counter with call" do
    {:ok, server} = GenServer.start(Counter, 0)
    new_count = GenServer.call(server, :inc)
    assert 1 == new_count and new_count == GenServer.call(server, :read)
  end

  test "Use multiple counters" do
    import Bitwise

    bound = 0..31

    assert bound
           |> Enum.map(fn i ->
             {:ok, server} = GenServer.start(Counter, 0)
             {i, server}
           end)
           |> Enum.map(fn {i, server} ->
             :ok = GenServer.cast(server, {:inc, 1 <<< i})
             server
           end)
           |> Enum.map(fn server ->
             1 == popcount(GenServer.call(server, :read))
           end)
           |> Enum.all?()
  end
end

defmodule StuffDictionaryTest do
  use ExUnit.Case, async: true

  test "Start server" do
    {:ok, server} = GenServer.start(StuffDictionary, %{})
    assert true == Process.alive?(server)
  end

  test "Key lookup" do
    {:ok, server} = GenServer.start(StuffDictionary, %{:hello => :world})
    assert :world == GenServer.call(server, {:lookup, :hello})
  end

  test "Key insert" do
    {:ok, server} = GenServer.start(StuffDictionary, %{})
    :ok = GenServer.cast(server, {:insert, :hello, :world})
    assert :world == GenServer.call(server, {:lookup, :hello})
  end

  test "Non-existant key" do
    {:ok, server} = GenServer.start(StuffDictionary, %{})
    assert nil == GenServer.call(server, {:lookup, :hello})
  end
end
