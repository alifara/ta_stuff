#!/usr/bin/env python
import numpy as np
from random import uniform, shuffle
import sys

T = 10
MAXN = 2000
only_MAXN = False


def gen_problem(n):
    remaining_idxs = [i for i in range(n)]
    shuffle(remaining_idxs)
    return remaining_idxs


def find_loop_len(i, p):
    # print(p)
    idxs = [i]
    nxt = p[i]
    while nxt != i:
        # print(i, p[i], p[p[i]], end=' ')
        idxs.append(nxt)
        nxt = p[nxt]
    # print(len(idxs), ': ', idxs)
    return idxs


def is_valid(p, hasEven=True):
    n = len(p)
    freq = [0 for i in range(n + 1)]
    seen_idx = set()

    for i in range(n):
        # print(seen_idx)
        # print(p)
        if i in seen_idx:
            continue
        idxs = find_loop_len(i, p)
        freq[len(idxs)] += 1
        for i in idxs:
            seen_idx.add(i)

    for i in range(1, n + 1):
        if i % 2 == 0:
            if freq[i] % 2 != 0:
                return False
            # if freq[i] != 0:
            # hasEven = True
    # if hasEven:
    # return True
    # return False
    return True


def main():
    for _ in range(T):
        if only_MAXN:
            n = MAXN
        else:
            n = round(uniform(3, MAXN))
        while True:
            x = gen_problem(n)
            # print(len(x))
            # print(*x)
            # break
            if is_valid(x, _ % 2 == 0):
                print(len(x))
                print(*x)
                print(f"{_+1}", file=sys.stderr, end="\r")
                break


if __name__ == "__main__":
    main()
