# Main

### Some Learning Resources
- [Elixir in Action 2nd Ed (Saša Jurić)](https://www.manning.com/books/elixir-in-action-second-edition)
	- Has my personal recommendation!
	- If you can't procure the book, [the code samples hosted on github](https://github.com/sasa1977/elixir-in-action) will suffice.
- [Joy of Elixir](https://joyofelixir.com/)
	- Short and sweet
	- Focuses on hands-on experience
	- Looks cool
- [Elixir Succinctly (Emanuele DelBono)](https://www.syncfusion.com/succinctly-free-ebooks/elixir-succinctly)
	Lives up to its name!
- [Elixir's Own Doc!!!](https://elixir-lang.org/getting-started/introduction.html)
	- Give a great overview of the language!
	- All examples are from this source.
- [Alchemist Camp](https://alchemist.camp/start)
	If you like learning via videos.

At least give Elixir website a visit, FFS 💢
https://elixir-lang.org/


### Session's Schedule

1. Intro
	- Why Elixir?
	- What is elixir?
	- What is Erlang made for?
		- telecom switches 
		- crash handling 
		- hardware/software fault 
		- seamless upgrades 
	- Environment's requirements?
		- soft-realtime 
		- high availability 
		- distributed
		- hot-swappable components
		- fault tolerance
	- VM? Python?


2. How to run Elixir?
	1. The Elixir shell `iex` 
		- help with `h`
		- `c("filename.ex")`
	2. The Elixir script runner `elixir`
		Files end with `.exs`.
	3. The Elixir compiler `elixirc` 
		Will create BEAM-VM compiled files with `.beam` extension.
	4. The software project management tool `mix` 
		- `mix help` 
		- `mix new` 
			- https://hexdocs.pm/mix/1.14/Mix.Tasks.New.html
				[hexdocs.pm!!!!!!](https://hexdocs.pm/)
		- Running project with `mix`
			lol idk

3. Syntax
	- `code/syntax.exs`
	- [Basic types](https://elixir-lang.org/getting-started/basic-types.html)
	- Modules
	- Functions
		- normal ones
		- inline
		- lambdas (fn ->  end)
		- defp
	- Multiple functions with same name but different input params
	- Pattern matching
	- `code/pattern.exs`
   

<!-- TODO create examples for these -->
4. Concurrency 
	1. what is a pid?
		- self()
		- pid object `h pid`
	2. what is a pid? x2
	3. spawn
	4. send
	5. receive
	6. `iex`'s flush
	7. examples
	8. how msg pipe works
	9. A process must be used to model runtime properties such as: (https://hexdocs.pm/elixir/1.14.3/library-guidelines.html#anti-patterns)
		- Mutable state and access to shared resources (such as ETS, files, and others)
		- Concurrency and distribution
		- Initialization, shutdown and restart logic (as seen in supervisors)
		- System messages such as timer messages and monitoring events
	10. [Agent](https://hexdocs.pm/elixir/1.14.3/Agent.html)

9999. Node

---

##### NOTE: 
- use elixir's help in front of them ( hexdocs.io, zeal, mac's dash )
